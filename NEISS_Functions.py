# -*- coding: utf-8 -*-
"""
Created on Mon Apr 12 14:20:05 2021

@author: rankmicha
"""

import pandas as pd
import numpy as np

lookups = pd.read_excel("NEISS_FMT.XLSX")

lookup = ["GENDER", "DIAG", "DISP", "BDYPT", "PROD", "PROD", "PROD", "LOC", "RACE"]
raw_data = ["Sex", "Diagnosis", "Disposition", "Body_Part", "Product_1", "Product_2", "Product_3", "Location", "Race"]

def replace_NEISS_codes(data):
    
    data_new = data.copy()
    
    for i, col in enumerate(lookup):
        code = lookups.loc[np.where(lookups['Format name'] == col)[0], ]
        replace_values = code['Format value label']
        for j, elem in replace_values.items():
            if isinstance(elem, str) and " - " in elem:
                replace_values.loc[j] = elem.split(" - ")[1]
        code.loc['Format value label'] = replace_values
        zip_iterator = zip(pd.to_numeric(code['Starting value for format']), code['Format value label'])
        code_dict = dict(zip_iterator)
        row = data_new.loc[:, raw_data[i]]
        row.replace(code_dict, inplace = True)
        data_new.loc[:, raw_data[i]] = row
    
    data_new['Month'] = pd.DatetimeIndex(data_new['Treatment_Date']).month
    data_new['Year'] = pd.DatetimeIndex(data_new['Treatment_Date']).year  
    
    return data_new
