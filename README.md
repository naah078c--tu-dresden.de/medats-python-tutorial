# Tutorial zur Datenverarbeitung in Python im Rahmen des WF Medizinische Data Science

Zur Nutzung sollte Python 3 bzw. Anaconda mit "Jupyter Notebooks" installiert sein.

Bei Fragen wenden Sie sich an sven.helfer@tu-dresden.de
